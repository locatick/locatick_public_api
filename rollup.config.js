import typescript from "rollup-plugin-typescript2";
import rollupNodeResolve from "rollup-plugin-node-resolve";
import rollupJSon from "rollup-plugin-json";
export default {
  input: "src/index.ts",
  output: [
    {
      file: "dist/index.js",
      format: "umd",
      name: "Locatick API",
      sourcemap: true,
    },
  ],
  plugins: [
    rollupJSon(),
    rollupNodeResolve({
      preferBuiltins: true,
      browser: false,
    }),
    typescript({
      tsconfigOverride: {
        exclude: ["**/*.spec.ts"],
      },
    }),
  ],
  external: ["axios"],
};
