import { LocatickApi } from "src/index";
import { ActivityStatus } from "src/types/activity";

const key = process.env.TOKEN;

describe("access", () => {
  it("gets some activities from server", async () => {
    if (key == null) return;
    const api = new LocatickApi(key);
    const res = await api.getActivities(
      {
        "status[]": [ActivityStatus.STATUS_NEW, ActivityStatus.STATUS_COMPLETED],
      },
      {
        rowsPerPage: 5,
        page: 1,
        sortBy: "id",
        descending: true,
      },
    );
    await res[0].getComments();
    // await res[0].getCustomFields();
    await res[0].getTags();
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    // await res[0].addComment("Komentarz", res[0].owner?.email ?? "");
    // const comments = await res[0].getComments();
    // console.log(comments);
    // expect(res.length).toBeGreaterThan(0);
    // const result = await api.addActivity({
    //   name: "Zadanie z API",
    //   description: "Opis",
    //   address:" Opole, Ozimska 4",
    //   number: "ZAD/2022/1",
    //   owner: {email: "jakub.konieczny@locatick.com" },
    //   category: { name: "Serwis"},
    //   object: {id: 5},
    //   tags: [{name: "Pilne"}],
    //   comments: [{comment: '', owner: '', date: ''}]
    // })
    // console.log(result)
  });
});
