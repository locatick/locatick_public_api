import { Model } from "src/types/common";

export type CustomFieldModel = Model & {
  name: string;
  value: string;
};
