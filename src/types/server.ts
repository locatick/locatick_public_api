import { Model, Stored } from "src/types/common";

export type StoredModel<T = any> = {
  id: number;
  "@id": string;
};

export type ApiResponse<T = any> = {
  "@context": string;
  "@id": string;
  "@type": string;
  "hydra:member": T extends Model ? Stored<T>[] : T[];
  "hydra:search"?: any;
  "hydra:totalItems"?: number;
  "hydra:view"?: {
    "@id": string;
    "@type": string;
    "hydra:first": string;
    "hydra:last": string;
    "hydra:next": string;
  };
};

export type Sort = {
  sortBy: string;
  descending?: boolean;
};

export type PaginationOnly = {
  page: number;
  rowsPerPage: number;
};

export type PaginationConfig = Partial<Sort> & Partial<PaginationOnly>;
