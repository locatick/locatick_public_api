import { Model } from "src/types/common";
import { UserModelEmbedded } from "src/types/user";

export type ActivityCommentModel = Model & {
  comment: string;
  owner: string; // ?
  date: string;
};

export type ActivityCommentPost = {
  comment: string;
  date: string | Date;
  owner: UserModelEmbedded;
};
