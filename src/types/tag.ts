import { Model } from "src/types/common";

export type TagModel = Model & {
  tagId: number;
  name: string;
};
