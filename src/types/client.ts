import { FilterType, Model } from "src/types/common";

export type ClientModel = Model & {
  name: string;
  city: string;
  address: string;
  email: string;
  phone: string;
  country: string;
  nip: string;
  regon: string;
  external_key: string;
};

export type ClientPost = {
  name: string;
  city: string;
  address: string;
  nip: string | null;
  regon: string | null;
  email: string;
  pcode: string | null;
  phone: string;
  country?: string | null;
  external_key: string;
};

export type ClientFilter = FilterType<{
  name: string;
  "name[]": string[];
  id: number;
  "id[]": number[];
  nip: string;
  "nip[]": string[];
  regon: string;
  "regon[]": string[];
  external_key: string;
  "external_key[]": string[];
}>;

// export class Client implements ClientModel {
//   id = null;
//   address = "";
//   city = "";
//   email = "";
//   externalKey = "";
//   name = "";
//   nip = "";
//   phone = "";
//   regon = "";
//   constructor(client: ClientModel) {
//     Object.assign(this, client);
//   }
//
//   async save() {
//     //
//   }
// }
