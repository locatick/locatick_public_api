import { FilterType, Model } from "src/types/common";
import { UserModelEmbedded } from "src/types/user";

type Contact = {
  name: string;
  phone: string;
  email: string;
  info: string;
};

export type ObjectModel = Model & {
  name: string;
  externalKey: null | string;
  client: UserModelEmbedded;
  address: {
    country: string;
    city: string;
    address: string;
    postCode: string;
    location: {
      lat: number;
      lng: number;
    };
  };
  contact: Contact;
  extraField1: string;
  extraField2: string;
  extraField3: string;
  extraField4: string;
  extraField5: string;
};

export type ObjectEmbedded = Model & {
  name: string;
};

export type ObjectDetailsModel = Model & {
  name: string;
  client: {
    id: number;
    name: string;
  };
};

export type ObjectPost = {
  name: string;
  external_key: string;
  address: null | {
    country: string | null;
    city: string | null;
    address: string | null;
    postCode: string | null;
    location: null | {
      lat: number;
      lng: number;
    };
  };
  contact: Contact | null;
  extraField1: string | null;
  extraField2: string | null;
  extraField3: string | null;
  extraField4: string | null;
  extraField5: string | null;
  client:
    | null
    | {
        id: number;
        external_key?: string | null;
      }
    | {
        id?: number | null;
        external_key: string;
      };
};

export type ObjectFilter = FilterType<{
  name: string;
  "name[]": string[];
  external_key: string;
  "external_key[]": string[];
}>;

// export class ObjectItem implements ObjectModel {
//   async update() {
//     //
//   }
// }
