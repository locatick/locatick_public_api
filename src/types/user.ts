import { Model } from "src/types/common";

export type UserModel = Model & {
  firstName: string;
  lastName: string;
  email: string;
};

export type UserModelEmbedded = { email: string };
