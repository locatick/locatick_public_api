import { FilterType, Model } from "src/types/common";

export type CategoryModel = Model & {
  name: string;
};

export type CategoryEmbedded = { name: string };

export type CategoryFilter = FilterType<{
  name: string;
  "name[]": string[];
  id: number;
  "id[]": number[];
}>;
