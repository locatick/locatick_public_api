export type Model = {
  "@id"?: string;
  "@type"?: string;
  id: number | null;
};

export type Stored<T extends Model> = T & {
  "@id": string;
  id: number;
};

export type FilterType<T = Record<string, any>> = {
  [K in keyof T]?: T[K] | null;
};
