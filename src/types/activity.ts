import { CategoryEmbedded, CategoryModel } from "src/types/category";
import { UserModel } from "src/types/user";
import { ObjectEmbedded, ObjectModel } from "src/types/object";
import { createUrl, locatickApi, locatickGetList, locatickPost } from "../api";
import { FilterType, Stored } from "src/types/common";
import { ActivityCommentModel, ActivityCommentPost } from "src/types/comment";
import { ApiResponse } from "src/types/server";
import { TagModel } from "src/types/tag";
import { CustomFieldModel } from "src/types/customField";

export enum ActivityStatus {
  STATUS_NEW = 10,
  STATUS_ASSIGNED = 40,
  STATUS_IN_PROGRES = 50,
  STATUS_TO_ACCEPT = 60,
  STATUS_COMPLETED = 20,
  STATUS_ARCHIVED = 30,
  STATUS_CANCELED = -10,

  STATUS_COMMISSIONED = 70,
  STATUS_REJECTED = 80,
}

export type ActivityModel = {
  "@id"?: string;
  id: number | null;
  name: string;
  description: string;
  address: string | null;
  // location: [number, number] | null;
  // city: string | null;
  // pcode: string | null;
  // country: string | null;
  // dueDate: string; // json date
  // flatNo: string | null;
  // isAllDay: boolean;
  // realizationDate: string; // json date
  // closeDate: string; // json date
  // createdAt: string; // json date
  // categoryName: string;
  // categoryId: number;
  // objectName: string | null;
  // objectId: number | null;
  // objectFk: string | null;
  owner: UserModel | null;
  category: CategoryModel;
  object: ObjectEmbedded | null;
};

export type ActivityFilter = FilterType<{
  "id[]": number[];
  name: string;
  "status[]": ActivityStatus[];
  "created_at[after]": string; // gte
  "created_at[before]": string; // lte
  "realization_date[after]": string; // gte
  "realization_date[before]": string; // lte
  "close_date[after]": string; // gte
  "close_date[before]": string; // lte
  "due_date[after]": string; // gte
  "due_date[before]": string; // lte
}>;

//
// export type ActivityDto = {
//   "@id"?: string | null;
//   id: number | null;
//   name: string;
//   address: string | null;
//   location: [number, number] | null;
//   description: string;
//   city: string | null;
//   pcode: string | null;
//   country: string | null;
//   dueDate: string; // json date
//   flatNo: string | null;
//   isAllDay: boolean;
//   realizationDate: string; // json date
//   closeDate: string; // json date
//   createdAt: string; // json date
//   categoryName: string;
//   categoryId: number;
//   objectName: string | null;
//   objectId: number | null;
//   objectFk: string | null;
//   tags: string[] | null;
// };

export type ActivityPost = {
  name: string;
  description: string;
  address: string;
  number: string;
  owner: { email: string };
  category: CategoryEmbedded;
  object: { id: number };
  tags?: Array<{ name: string }>;
  comments?: Array<{ comment: string; owner: string; date: Date|string }>;
  customFields?: Array<{ name: string; value: string }>;
};
export const ActivityPostBase: Partial<ActivityPost> = {
  tags: [],
  comments: [],
  customFields: [],
}

/**
 * Klasa-kontroler dla zadań
 */
export class Activity implements ActivityModel {
  "@id" = undefined;
  address = null;
  category;
  description = "";
  id = null;
  name = "";
  object = null;
  owner = null;

  constructor(model: Partial<ActivityModel> & Pick<ActivityModel, "category">) {
    this.category = model.category;
    Object.assign(this, model);
  }

  /**
   * Zapisz zmiany
   * @returns model zapisanego zadania
   */
  async save() {
    if (this.isStored) return;
    return locatickPost(`activities/${this.id}`, this);
  }

  /**
   * Pobiera przypisanych do zadania użytkowników
   * @returns lista wykonawców zadania {@link UserModel}
   */
  async getAssignedUsers() {
    if (!this.isStored) return [];
    return locatickGetList<UserModel>(`activities/${this.id}/assigned-users`);
  }

  /**
   * Przypisuje użytkownika do zadania
   * @param user {@link UserModel} lub string z emailem
   */
  async addAssignedUser(user: UserModel | string) {
    if (!this.isStored) return [];
    await locatickPost(`activities/${this.id}/assigned-users`, {
      email: typeof user == "string" ? user : user.email,
    });
  }

  /**
   * Pobiera komentarze
   * @returns lista komentarzy {@link ActivityCommentModel}
   */
  async getComments(): Promise<Stored<ActivityCommentModel>[]> {
    if (!this.isStored) return [];
    return locatickGetList<ActivityCommentModel>(`activities/${this.id}/comments`);
  }

  /**
   * Dodaje komentarz
   * @param comment - treść komentarza
   * @param owner - autor komentarza {@link UserModel} lub email
   * @param date - data (opcjonalnie, obiekt Date)
   */
  async addComment(comment: string, owner: UserModel | string, date?: Date) {
    if (!this.isStored) return [];
    await locatickPost(`activities/${this.id}/comments`, <ActivityCommentPost>{
      comment,
      owner: { email: typeof owner == "string" ? owner : owner.email },
      date: date ?? new Date(),
    });
  }

  /**
   * Pobiera listę pól dodatkowych
   * @returns lista pól dodatkowych {@link CustomFieldModel}
   */
  async getCustomFields(): Promise<Stored<CustomFieldModel>[]> {
    if (!this.isStored) return [];
    return locatickGetList<CustomFieldModel>(`activities/${this.id}/custom-fields`);
  }

  /**
   * Dodaje pole dodatkowe
   * @param name - nazwa pola dodatkowego
   * @param value - wartość pola dodatkowego
   * @returns
   */
  async addCustomField(name: string, value: string | number) {
    if (!this.isStored) return [];
    return await locatickPost(`activities/${this.id}/custom-fields`, {
      name,
      value,
    });
  }

  async getTags() {
    if (!this.isStored) return [];
    return locatickGetList<TagModel>(`activities/${this.id}/tags`);
  }

  async addTag(name: string) {
    if (!this.isStored) return [];
    return await locatickPost(`activities/${this.id}/tags`, {
      name,
    });
  }

  get isStored(): boolean {
    return this.id != null;
  }
}

export function parseActivityFromResponse(src: ActivityModel) {
  return new Activity(src);
}
