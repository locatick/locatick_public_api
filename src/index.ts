import {
  Activity,
  ActivityFilter,
  ActivityModel,
  ActivityPost, ActivityPostBase,
  ActivityStatus,
  parseActivityFromResponse,
} from "src/types/activity";
import { PaginationConfig } from "src/types/server";
import { urlBuilder } from "src/utilities/UrlBuilder";
import {createUrl, locatickApi, locatickGetList, locatickContext, locatickPost, locatickPut} from "src/api";
import { CategoryFilter, CategoryModel } from "src/types/category";
import { FilterType, Stored } from "src/types/common";
import { ClientFilter, ClientModel, ClientPost } from "src/types/client";
import { ObjectFilter, ObjectModel, ObjectPost } from "src/types/object";

export {
  Activity,
  ActivityFilter,
  ActivityPost,
  ActivityStatus,
  ObjectPost,
  ClientPost,
  ClientFilter,
};

export class LocatickApi {
  constructor(private key: string) {
    this.setActive();
  }

  setActive() {
    locatickContext.headers.Authorization = this.key;
  }

  setUrl(url: string) {
    locatickContext.baseUrl = url;
  }

  /*  ACTIVITIES  */
  /**
   * Pobiera zadania
   * @param filter - filtry {@link ActivityFilter}
   * @param pagination - ustawienia paginacji {@link PaginationConfig}
   * @returns lista zadań (kontrolery {@link Activity})
   */
  async getActivities(filter?: ActivityFilter, pagination?: PaginationConfig): Promise<Activity[]> {
    const params = urlBuilder({ filter, pagination });
    const url = createUrl("activities", params);
    const rawList = await locatickGetList<ActivityModel>(url.toString());
    return rawList.map(parseActivityFromResponse);
  }

  /**
   * Pobiera zadanie
   * @param id - id zadania
   * @returns zadanie (kontroler {@link Activity})
   */
  public async getActivity(id: number): Promise<Activity> {
    const res = await locatickApi.get<ActivityModel>(createUrl(`activities/${id}`).toString());
    return parseActivityFromResponse(res.data);
  }

  /**
   * Dodaje zadanie
   * @param activity Dodanie zadania {@link ActivityPost} {
   *   name: string;
   *   description: string;
   *   address: string;
   *   number: string;
   *   owner: { name: string };
   *   category: { name: string };
   *   object: { id: number };
   *   tags: Array<{ name: string }>;
   *   comments: Array<{ comment: string; owner: string; date: Date }>;
   *   customFields: Array<{ name: string; value: string }>;
   * }
   * @returns utworzone zadanie (kontroler {@link Acitivty})
   */
  public async addActivity(activity: ActivityPost) {
    const res = await locatickApi.post<Activity, ActivityPost>(
      createUrl(`activities`).toString(),
      {...ActivityPostBase, ...activity},
    );
    return parseActivityFromResponse(res.data);
  }

  /*  CATEGORY  */
  /**
   * Pobiera kategorie
   * @param filter - filtry
   * @param pagination - ustawienia paginacji {@link PaginationConfig}
   * @returns lista kategorii
   */
  public async getCategories(
    filter?: FilterType,
    pagination?: PaginationConfig,
  ): Promise<Stored<CategoryModel>[]> {
    const params = urlBuilder({ filter, pagination });
    return await locatickGetList<CategoryModel>(createUrl("categories", params).toString());
  }

  /**
   * Pobiera kategorię wg ID
   * @param id id kategorii
   * @returns kategoria
   */
  public async getCategory(id: number): Promise<Stored<CategoryModel>> {
    return (await locatickApi.get<Stored<CategoryModel>>(`categories/${id}`)).data;
  }

  /*  CLIENT  */
  /**
   * Pobiera klientów
   * @param filter - filtry
   * @param pagination - ustawienia paginacji {@link PaginationConfig}
   * @returns lista klientów
   */
  public async getClients(filter?: CategoryFilter, pagination?: PaginationConfig) {
    const params = urlBuilder({ filter, pagination });
    return await locatickGetList<ClientModel>(createUrl("clients", params).toString());
  }

  /**
   * Pobiera klienta wg id
   * @param id - id klienta
   * @returns klient
   */
  public async getClient(id: number): Promise<Stored<ClientModel>> {
    return (await locatickApi.get<Stored<ClientModel>>(`clients/${id}`)).data;
  }

  /**
   * Dodaje klienta do bazy
   * @param client opis klienta: {@link ClientPost} {
   *   name: string;
   *   city: string;
   *   address: string;
   *   nip: string | null;
   *   regon: string | null;
   *   email: string;
   *   pcode: string;
   *   phone: string;
   *   external_key: string;
   * };
   * @returns utworzona encja
   */
  public async addClient(client: ClientPost) {
    const res = await locatickPost<Stored<ClientModel>, ClientPost>("clients", client);
    return res.data;
  }

  /*  OBJECTS  */
  /**
   * Pobiera obiekty
   * @param filter - filtry
   * @param pagination  - ustawienia paginacji {@link PaginationConfig}
   * @returns lista obiektów
   */
  public async getObjects(filter?: ObjectFilter, pagination?: PaginationConfig) {
    const params = urlBuilder({ filter, pagination });
    return await locatickGetList<ObjectModel>(createUrl("objects", params).toString());
  }

  /**
   * Pobiera obiekt
   * @param id - id obiektu
   * @returns obiekt
   */
  public async getObject(id: number): Promise<Stored<ObjectModel>> {
    return (await locatickApi.get<Stored<ObjectModel>>(`objects/${id}`)).data;
  }

  /**
   * Dodaje obiekt
   * @param obj - dane obiektu do dodania {@link ObjectPost} {
   *   name: string;
   *   external_key: string;
   *   address: {
   *     country: string;
   *     city: string;
   *     address: string;
   *     postCode: string;
   *   };
   *   contact: {
   *     name: string;
   *     phone: string;
   *     email: string;
   *     info: string;
   *   };
   * }
   * @returns utworzony obiekt
   */
  public async addObject(obj: ObjectPost) {
    const res = await locatickPost<Stored<ObjectModel>, ObjectPost>("objects", obj);
    return res.data;
  }

  /**
   * Pobiera obiekt po kluczu obcym
   * @param externalKey - klucz obcy
   */
  public async getObjectByExternalKey(externalKey: string, ) {
    const res = await locatickApi.get<Stored<ObjectModel>>(`objectsByExternalKey/${externalKey}`);
    return res.data;
  }

    /**
   * Aktualizuje obiekt
   * @param externalKey - klucz obcy
   * @param obj - dane obiektu do dodania {@link ObjectPost} {
   *   name: string;
   *   external_key: string;
   *   address: {
   *     country: string;
   *     city: string;
   *     address: string;
   *     postCode: string;
   *   };
   *   contact: {
   *     name: string;
   *     phone: string;
   *     email: string;
   *     info: string;
   *   };
   * }
   * @returns zaktualizowany obiekt
   */
  public async updateObjectByExternalKey(externalKey: string, obj: ObjectPost) {
    const res = await locatickPut<Stored<ObjectModel>, ObjectPost>(`objects/externalKey/${externalKey}`, obj);
    return res.data;
  }

  /**
   * Aktualizuje obiekt
   * @returns zaktualizowany obiekt
   */
  public async updateObject(obj: Stored<ObjectModel>) {
    const res = await locatickPut<Stored<ObjectModel>, Stored<ObjectModel>>(`objects/${obj.id}`, obj);
    return res.data;
  }
}
