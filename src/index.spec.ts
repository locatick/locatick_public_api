import { LocatickApi } from "src/index";
import { ActivityStatus } from "src/types/activity";
import { locatickApi } from "src/api";
import { createServer, Model } from "miragejs";

let id = 0;
const ActivityFixture = (_id?: number) => {
  const i = _id ?? id++;
  return {
    "@id": `/activity/${i}`,
    "@type": "/activity",
    "@context": "",
    id: i,
    name: "Task",
    address: "Unknown",
    location: [52, 21],
    description: "",
    city: "string",
    pcode: "",
    status: ActivityStatus.STATUS_NEW,
    country: "",
    isAllDay: true,
    dueDate: "2022-05-05T15:00:00Z", //date
    flatNo: "",
  };
};

describe.skip("api", () => {
  let server: any;

  beforeEach(() => {
    // const s = createServer({
    //   environment: "test",
    //   routes() {
    //     this.get("/activities/5", (props) => {
    //       console.log(props);
    //       return ActivityFixture();
    //     });
    //     this.get("/activities", () => [ActivityFixture(), ActivityFixture()]);
    //   },
    // });
    // server = s;
  });

  afterEach(() => {
    server?.shutdown();
    server = null;
  });

  it("Sends auth", async () => {
    const api = new LocatickApi("TEST");
    const getter = jest.fn(() => ActivityFixture(5));
    server = createServer({
      environment: "test",
      urlPrefix: "http://test.api.locatick.cloud",
      namespace: "/api/public",
      routes() {
        this.get("/activities/5", getter);
      },
    });
    const activity = await api.getActivity(5);
    expect(activity).not.toBeNull();
    console.log(activity);
    // expect(activity?.id).toBe(5);
    expect(getter).toBeCalled();
  });

  // it("Gets by id", async () => {
  //   const api = new LocatickApi("TEST");
  //   mock.onGet("/activities/5").reply(200, ActivityFixture());
  //   await api.getActivity(5);
  //   expect(mock.history.get[0].url).toEqual(`activities/5`);
  // });

  // it("Gets filtered and paginated", async () => {
  //   const api = new LocatickApi("TEST");
  //   mock.onGet(/activities/).reply(200, {
  //     "hydra:member": [ActivityFixture(), ActivityFixture()],
  //   });
  //   const res = await api.getActivities(
  //     {
  //       "status[]": [ActivityStatus.STATUS_NEW, ActivityStatus.STATUS_ASSIGNED],
  //     },
  //     {
  //       page: 1,
  //       rowsPerPage: 10,
  //       sortBy: "id",
  //       descending: true,
  //     },
  //   );
  //   const params = new URLSearchParams(mock.history.get[0].url);
  //   // console.log(params);
  //   expect(mock.history.get[0].url?.startsWith("activities")).toBeTruthy();
  //   expect(params.getAll("status[]").length).toBe(2);
  //   expect(params.getAll("status[]")).toContain(ActivityStatus.STATUS_NEW.toString());
  //   expect(params.getAll("status[]")).toContain(ActivityStatus.STATUS_ASSIGNED.toString());
  //   expect(params.get("order[id]")).toEqual("desc");
  // });
});
