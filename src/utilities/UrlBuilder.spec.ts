import { clearFilter, FetchParamsInterface, urlBuilder } from "./UrlBuilder";

describe("UrlSearchParamsBuilder", () => {
  it("creates emtyp params", () => {
    const result = urlBuilder();
    expect(result).toBeTruthy();
  });
  it("creates valid filters", () => {
    const params: FetchParamsInterface = {
      filter: {
        text: "foo",
        integer: 1,
        integerZero: 0,
        arrayInteger: [1, 2, 3],
        scheduledFalse: false,
        scheduledTrue: true,
        removeNull: null,
        removeUndef: undefined,
        emptyArray: [],
        emptyString: "",
        iriContainer: {
          "@id": "/item/1",
        } as any, //intentional illegal cast!
        iriContainerArray: [
          {
            "@id": "/item/1",
          },
          {
            "@id": "/item/2",
          },
          {
            "@id": "/item/3",
          },
        ] as any, //intentional illegal cast!
      },
    };
    const result = urlBuilder(params);
    expect(result.get("text")).toBe("foo");
    expect(result.get("integer")).toBe("1");
    expect(result.get("integerZero")).toBe("0");
    expect(result.getAll("arrayInteger")).toEqual(["1", "2", "3"]);
    expect(result.get("scheduledFalse")).toBe("false");
    expect(result.get("scheduledTrue")).toBe("true");
    expect(result.get("removeNull")).toBe(null);
    expect(result.get("removeUndef")).toBe(null);
    expect(result.get("emptyArray")).toBe(null);
    expect(result.get("emptyString")).toBe(null);
    expect(result.get("iriContainer")).toBe("1");
    expect(result.getAll("iriContainerArray")).toEqual(["1", "2", "3"]);
  });

  it("creates valid sort", () => {
    const paginator = {
      rowsPerPage: 10,
      sortBy: "id",
      descending: true,
      page: 1,
    };
    const params: FetchParamsInterface = {
      pagination: paginator,
    };
    const result = urlBuilder(params);
    expect(result.get("page")).toBe(paginator.page.toString());
    expect(result.get("itemsPerPage")).toBe(paginator.rowsPerPage.toString());
    expect(result.get(`order[${paginator.sortBy}]`)).toBe("desc");
  });

  it("clears filters", () => {
    const input = {
      foo: "foo",
      bar: null,
      val: 3,
      arr: [1, 2, 3],
    };
    const res = clearFilter(input);
    expect(Object.keys(res).length).toBe(3);
    expect(res["foo"]).toBe(input.foo);
    expect(res["val"]).toBe(input.val);
    expect(res["arr"]?.length).toBe(3);
  });
});
