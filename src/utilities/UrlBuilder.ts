import { PaginationConfig } from "src/types/server";
import { Model } from "src/types/common";

export interface FetchParamsInterface {
  pagination?: PaginationConfig;
  filter?: Record<
    string,
    string | string[] | number | number[] | boolean | null | undefined
  > | null;
}

const idFromIri = (iri: string) => iri.split("/").slice(-1)[0];

const camelToUnderscore = (key: string) => {
  return key.replace(/([A-Z])/g, "_$1").toLowerCase();
};

const clearNested = (field: Record<string, any> | any[]): any => {
  if (field == null) return null;
  if (Array.isArray(field)) return field.map((i) => clearNested(i));
  if (typeof field != "object") return field;
  return Object.entries(field).reduce((res, [key, value]) => {
    if (Array.isArray(value)) {
      return {
        ...res,
        [key]: value.map((i: Model) => i["@id"] ?? i.id ?? i),
      };
    }
    return {
      ...res,
      [key]: typeof value == "object" ? clearNested(value) : value,
    };
  }, {});
};

export const clearFilter = <T = Record<string, any>>(filter: T): Partial<T> => {
  return Object.entries(filter).reduce((res, [key, value]) => {
    if (value != null) {
      return {
        ...res,
        [key]: clearNested(value),
      };
    }
    return res;
  }, {});
};

export const urlBuilder = (params?: FetchParamsInterface | null): URLSearchParams => {
  const searchParams = new URLSearchParams();

  if (params == null) {
    return searchParams;
  }

  if (params.pagination?.page) {
    searchParams.append("page", params.pagination.page.toString());
    searchParams.append("itemsPerPage", params.pagination.rowsPerPage?.toString() ?? "30");
  } else {
    searchParams.append("pagination", "false");
  }
  if (params.pagination?.sortBy) {
    searchParams.append(
      "order[" + camelToUnderscore(params.pagination.sortBy.toString()) + "]",
      params.pagination.descending ?? false ? "desc" : "asc",
    );
  }

  if (params.filter) {
    for (const key in params.filter) {
      if (Object.prototype.hasOwnProperty.call(params.filter, key)) {
        const value = params.filter[key];
        if (typeof value === "string" || Array.isArray(value)) {
          if (value.length == 0) {
            continue;
          }
        }
        if (value != null) {
          if (Array.isArray(value)) {
            value.forEach((data: any) => {
              if (typeof data === "object" && "@id" in data) {
                searchParams.append(key, idFromIri(data["@id"]));
              } else {
                searchParams.append(key, data);
              }
            });
          } else if (typeof value === "object") {
            if ("@id" in value) {
              searchParams.append(key, idFromIri(value["@id"]));
            }
          } else {
            searchParams.append(key, value.toString());
          }
        }
      }
    }
  }
  return searchParams;
};
