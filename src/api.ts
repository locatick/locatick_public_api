import { ApiResponse } from "src/types/server";
import { ActivityCommentModel } from "src/types/comment";
import { Model, Stored } from "src/types/common";

export const locatickContext = {
  baseUrl: process.env.LOCATICK_URL ?? "https://api.locatick.cloud",
  headers: {
    Authorization: "",
  },
};

export type Response<T> = {
  data: T;
  status: number;
};

function mergeUrl(url: string) {
  if (locatickContext.baseUrl.endsWith("/")) return `${locatickContext.baseUrl}api/public/${url}`;
  return `${locatickContext.baseUrl}/api/public/${url}`;
}

export const locatickApi = {
  get: async <T>(url: string): Promise<Response<T>> => {
    const headers = {
        "Content-Type": "application/json",
        ...locatickContext.headers,
      }
    const request = await fetch(mergeUrl(url), {
      method: "GET",
      headers,
    });
    try {
      const res = (await request.json());
      return {
        data: res,
        status: request.status,
      };
    } catch(err) {
      console.error(err)
      throw err;
    }
  },
  post: async <T = any, D = T>(url: string, body?: D): Promise<Response<T>> => {
    const _body = body ? JSON.stringify(body) : ""
    const request = await fetch(mergeUrl(url), {
      method: "POST",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
        ...locatickContext.headers,
      },
      body: _body
    });
    if (request.status >= 400) {
      const res= await request.text();
      throw new Error(res)
    }
    const res: T = await request.json();
    return {
      data: res,
      status: request.status,
    };
  },
  put: async <T = any, D = T>(url: string, body?: D): Promise<Response<T>> => {
    const _body = body ? JSON.stringify(body) : ""
    const request = await fetch(mergeUrl(url), {
      method: "PUT",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
        ...locatickContext.headers,
      },
      body: _body
    });
    if (request.status >= 400) {
      const res= await request.text();
      throw new Error(res)
    }
    const res: T = await request.json();
    return {
      data: res,
      status: request.status,
    };
  },
};

export function createUrl(endpoint: string, params?: URLSearchParams): string {
  if (params) {
    return `${endpoint}?${params?.toString() ?? ""}`;
  } else {
    return endpoint;
  }
}

export async function locatickGetList<T extends Model>(
  url: string,
  params?: URLSearchParams,
): Promise<Stored<T>[]> {
  const res = await locatickApi.get<ApiResponse<Stored<T>>>(createUrl(url, params).toString());
  return res.data["hydra:member"];
}

export async function locatickPost<T extends Model, D = any>(
  url: string,
  body?: D,
): Promise<Response<T>> {
  return await locatickApi.post<T, D>(url, body);
}


export async function locatickPut<T extends Model, D = any>(
  url: string,
  body?: D,
): Promise<Response<T>> {
  return await locatickApi.put<T, D>(url, body);
}
