// eslint-disable-next-line @typescript-eslint/no-var-requires
require("dotenv").config();
module.exports = {
  preset: "ts-jest",
  testEnvironment: "node",
  testMatch: ["<rootDir>/src/**/*.spec.js", "<rootDir>/src/**/*.spec.ts"],
  globals: {
    "ts-jest": {
      tsconfig: "tsconfig.json",
    },
  },
  transform: {
    "^.+\\.ts?$": "ts-jest",
  },
  roots: ["src"],
  moduleFileExtensions: ["js", "ts"],
  moduleDirectories: ["node_modules", "src"],
  moduleNameMapper: {
    "^~/(.*)$": "<rootDir>/$1",
    "^/(.*)$": "<rootDir>/$1",
    "^src/(.*)$": "<rootDir>/src/$1",
  },
  coveragePathIgnorePatterns: ["node_modules", "src/test", "src/types"],
  transformIgnorePatterns: ["node_modules/(?!(node-fetch)/)"],
};
