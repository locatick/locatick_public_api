# Locatick Public API

## Utworzenie obiektu API:
```ts
const api = new LocatickApi("TOKEN");
```

## Pobranie zadań

```ts
const api = new LocatickApi("TOKEN");
const filters = {
    "status[]": [ActivityStatus.NEW],
}
const activities = await api.getActivities(
    filters,
    { page: 1, rowsPerPage: 10 }
);
```

## Pobranie jednego konkretnego zadania
```ts
const activity =  await api.getActivity(57975);
```
## Dodanie komentarza/tagu

```ts
activity.addTag("Pilne");
activity.addComment("Wykonać pilnie", "user@mail.com");
```

## Dodanie klienta
```ts
await api.addClient({
    name: 'Klient z API',
    city: 'API',
    address: '',
    nip: null,
    regon: null,
    email: 'adres@email.com',
    pcode: '',
    phone: '',
    external_key: 'api1',
  })
```

## Dodanie obiektu
```ts
const objectItem = await api.addObject({  
    name: "Obiekt z API",
    external_key: "obj_api_1",
    address: {
      country: "Polska",
      city: "Kraków",
      address: "Kraków, Katowicka 1",
      postCode: "31-351",
    },
    contact: null,
})
```
## Dodanie zadania
```ts
await api.addActivity({
    name: "Zadanie z API",
    description: "Opis z API",
    address: "Kraków, Katowicka 1",
    number: "ZAD/2022/1",
    owner: { email: 'user@mail.com' },
    category: {name: 'Serwery' },
    object: { id: objectItem.id },
    tags: [{name: 'Pilne'}],
    // comments: Array<{ comment: string; owner: string; date: Date }>,
    // customFields: Array<{ name: string; value: string }>,
})
```
