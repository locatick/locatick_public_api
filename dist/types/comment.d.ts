import { Model } from "src/types/common";
import { UserModelEmbedded } from "src/types/user";
export declare type ActivityCommentModel = Model & {
    comment: string;
    owner: string;
    date: string;
};
export declare type ActivityCommentPost = {
    comment: string;
    date: string | Date;
    owner: UserModelEmbedded;
};
