import { Model, Stored } from "src/types/common";
export declare type StoredModel<T = any> = {
    id: number;
    "@id": string;
};
export declare type ApiResponse<T = any> = {
    "@context": string;
    "@id": string;
    "@type": string;
    "hydra:member": T extends Model ? Stored<T>[] : T[];
    "hydra:search"?: any;
    "hydra:totalItems"?: number;
    "hydra:view"?: {
        "@id": string;
        "@type": string;
        "hydra:first": string;
        "hydra:last": string;
        "hydra:next": string;
    };
};
export declare type Sort = {
    sortBy: string;
    descending?: boolean;
};
export declare type PaginationOnly = {
    page: number;
    rowsPerPage: number;
};
export declare type PaginationConfig = Partial<Sort> & Partial<PaginationOnly>;
