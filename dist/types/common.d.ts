export declare type Model = {
    "@id"?: string;
    "@type"?: string;
    id: number | null;
};
export declare type Stored<T extends Model> = T & {
    "@id": string;
    id: number;
};
export declare type FilterType<T = Record<string, any>> = {
    [K in keyof T]?: T[K] | null;
};
