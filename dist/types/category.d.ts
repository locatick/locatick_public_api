import { FilterType, Model } from "src/types/common";
export declare type CategoryModel = Model & {
    name: string;
};
export declare type CategoryEmbedded = {
    name: string;
};
export declare type CategoryFilter = FilterType<{
    name: string;
    "name[]": string[];
    id: number;
    "id[]": number[];
}>;
