import { Model } from "src/types/common";
export declare type CustomFieldModel = Model & {
    name: string;
    value: string;
};
