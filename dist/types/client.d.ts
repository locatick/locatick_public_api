import { FilterType, Model } from "src/types/common";
export declare type ClientModel = Model & {
    name: string;
    city: string;
    address: string;
    email: string;
    phone: string;
    country: string;
    nip: string;
    regon: string;
    external_key: string;
};
export declare type ClientPost = {
    name: string;
    city: string;
    address: string;
    nip: string | null;
    regon: string | null;
    email: string;
    pcode: string | null;
    phone: string;
    country?: string | null;
    external_key: string;
};
export declare type ClientFilter = FilterType<{
    name: string;
    "name[]": string[];
    id: number;
    "id[]": number[];
    nip: string;
    "nip[]": string[];
    regon: string;
    "regon[]": string[];
    external_key: string;
    "external_key[]": string[];
}>;
