import { Model } from "src/types/common";
export declare type UserModel = Model & {
    firstName: string;
    lastName: string;
    email: string;
};
export declare type UserModelEmbedded = {
    email: string;
};
