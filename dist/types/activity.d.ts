import { CategoryEmbedded, CategoryModel } from "src/types/category";
import { UserModel } from "src/types/user";
import { ObjectEmbedded } from "src/types/object";
import { FilterType, Stored } from "src/types/common";
import { ActivityCommentModel } from "src/types/comment";
import { TagModel } from "src/types/tag";
import { CustomFieldModel } from "src/types/customField";
export declare enum ActivityStatus {
    STATUS_NEW = 10,
    STATUS_ASSIGNED = 40,
    STATUS_IN_PROGRES = 50,
    STATUS_TO_ACCEPT = 60,
    STATUS_COMPLETED = 20,
    STATUS_ARCHIVED = 30,
    STATUS_CANCELED = -10,
    STATUS_COMMISSIONED = 70,
    STATUS_REJECTED = 80
}
export declare type ActivityModel = {
    "@id"?: string;
    id: number | null;
    name: string;
    description: string;
    address: string | null;
    owner: UserModel | null;
    category: CategoryModel;
    object: ObjectEmbedded | null;
};
export declare type ActivityFilter = FilterType<{
    "id[]": number[];
    name: string;
    "status[]": ActivityStatus[];
    "created_at[after]": string;
    "created_at[before]": string;
    "realization_date[after]": string;
    "realization_date[before]": string;
    "close_date[after]": string;
    "close_date[before]": string;
    "due_date[after]": string;
    "due_date[before]": string;
}>;
export declare type ActivityPost = {
    name: string;
    description: string;
    address: string;
    number: string;
    owner: {
        email: string;
    };
    category: CategoryEmbedded;
    object: {
        id: number;
    };
    tags?: Array<{
        name: string;
    }>;
    comments?: Array<{
        comment: string;
        owner: string;
        date: Date | string;
    }>;
    customFields?: Array<{
        name: string;
        value: string;
    }>;
};
export declare const ActivityPostBase: Partial<ActivityPost>;
/**
 * Klasa-kontroler dla zadań
 */
export declare class Activity implements ActivityModel {
    "@id": undefined;
    address: null;
    category: import("src/types/common").Model & {
        name: string;
    };
    description: string;
    id: null;
    name: string;
    object: null;
    owner: null;
    constructor(model: Partial<ActivityModel> & Pick<ActivityModel, "category">);
    /**
     * Zapisz zmiany
     * @returns model zapisanego zadania
     */
    save(): Promise<import("../api").Response<import("src/types/common").Model> | undefined>;
    /**
     * Pobiera przypisanych do zadania użytkowników
     * @returns lista wykonawców zadania {@link UserModel}
     */
    getAssignedUsers(): Promise<Stored<UserModel>[]>;
    /**
     * Przypisuje użytkownika do zadania
     * @param user {@link UserModel} lub string z emailem
     */
    addAssignedUser(user: UserModel | string): Promise<never[] | undefined>;
    /**
     * Pobiera komentarze
     * @returns lista komentarzy {@link ActivityCommentModel}
     */
    getComments(): Promise<Stored<ActivityCommentModel>[]>;
    /**
     * Dodaje komentarz
     * @param comment - treść komentarza
     * @param owner - autor komentarza {@link UserModel} lub email
     * @param date - data (opcjonalnie, obiekt Date)
     */
    addComment(comment: string, owner: UserModel | string, date?: Date): Promise<never[] | undefined>;
    /**
     * Pobiera listę pól dodatkowych
     * @returns lista pól dodatkowych {@link CustomFieldModel}
     */
    getCustomFields(): Promise<Stored<CustomFieldModel>[]>;
    /**
     * Dodaje pole dodatkowe
     * @param name - nazwa pola dodatkowego
     * @param value - wartość pola dodatkowego
     * @returns
     */
    addCustomField(name: string, value: string | number): Promise<never[] | import("../api").Response<import("src/types/common").Model>>;
    getTags(): Promise<Stored<TagModel>[]>;
    addTag(name: string): Promise<never[] | import("../api").Response<import("src/types/common").Model>>;
    get isStored(): boolean;
}
export declare function parseActivityFromResponse(src: ActivityModel): Activity;
