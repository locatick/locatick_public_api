import { Model } from "src/types/common";
export declare type TagModel = Model & {
    tagId: number;
    name: string;
};
