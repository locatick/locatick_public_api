import { Activity, ActivityFilter, ActivityPost, ActivityStatus } from "src/types/activity";
import { PaginationConfig } from "src/types/server";
import { CategoryFilter, CategoryModel } from "src/types/category";
import { FilterType, Stored } from "src/types/common";
import { ClientFilter, ClientModel, ClientPost } from "src/types/client";
import { ObjectFilter, ObjectModel, ObjectPost } from "src/types/object";
export { Activity, ActivityFilter, ActivityPost, ActivityStatus, ObjectPost, ClientPost, ClientFilter, };
export declare class LocatickApi {
    private key;
    constructor(key: string);
    setActive(): void;
    setUrl(url: string): void;
    /**
     * Pobiera zadania
     * @param filter - filtry {@link ActivityFilter}
     * @param pagination - ustawienia paginacji {@link PaginationConfig}
     * @returns lista zadań (kontrolery {@link Activity})
     */
    getActivities(filter?: ActivityFilter, pagination?: PaginationConfig): Promise<Activity[]>;
    /**
     * Pobiera zadanie
     * @param id - id zadania
     * @returns zadanie (kontroler {@link Activity})
     */
    getActivity(id: number): Promise<Activity>;
    /**
     * Dodaje zadanie
     * @param activity Dodanie zadania {@link ActivityPost} {
     *   name: string;
     *   description: string;
     *   address: string;
     *   number: string;
     *   owner: { name: string };
     *   category: { name: string };
     *   object: { id: number };
     *   tags: Array<{ name: string }>;
     *   comments: Array<{ comment: string; owner: string; date: Date }>;
     *   customFields: Array<{ name: string; value: string }>;
     * }
     * @returns utworzone zadanie (kontroler {@link Acitivty})
     */
    addActivity(activity: ActivityPost): Promise<Activity>;
    /**
     * Pobiera kategorie
     * @param filter - filtry
     * @param pagination - ustawienia paginacji {@link PaginationConfig}
     * @returns lista kategorii
     */
    getCategories(filter?: FilterType, pagination?: PaginationConfig): Promise<Stored<CategoryModel>[]>;
    /**
     * Pobiera kategorię wg ID
     * @param id id kategorii
     * @returns kategoria
     */
    getCategory(id: number): Promise<Stored<CategoryModel>>;
    /**
     * Pobiera klientów
     * @param filter - filtry
     * @param pagination - ustawienia paginacji {@link PaginationConfig}
     * @returns lista klientów
     */
    getClients(filter?: CategoryFilter, pagination?: PaginationConfig): Promise<Stored<ClientModel>[]>;
    /**
     * Pobiera klienta wg id
     * @param id - id klienta
     * @returns klient
     */
    getClient(id: number): Promise<Stored<ClientModel>>;
    /**
     * Dodaje klienta do bazy
     * @param client opis klienta: {@link ClientPost} {
     *   name: string;
     *   city: string;
     *   address: string;
     *   nip: string | null;
     *   regon: string | null;
     *   email: string;
     *   pcode: string;
     *   phone: string;
     *   external_key: string;
     * };
     * @returns utworzona encja
     */
    addClient(client: ClientPost): Promise<Stored<ClientModel>>;
    /**
     * Pobiera obiekty
     * @param filter - filtry
     * @param pagination  - ustawienia paginacji {@link PaginationConfig}
     * @returns lista obiektów
     */
    getObjects(filter?: ObjectFilter, pagination?: PaginationConfig): Promise<Stored<ObjectModel>[]>;
    /**
     * Pobiera obiekt
     * @param id - id obiektu
     * @returns obiekt
     */
    getObject(id: number): Promise<Stored<ObjectModel>>;
    /**
     * Dodaje obiekt
     * @param obj - dane obiektu do dodania {@link ObjectPost} {
     *   name: string;
     *   external_key: string;
     *   address: {
     *     country: string;
     *     city: string;
     *     address: string;
     *     postCode: string;
     *   };
     *   contact: {
     *     name: string;
     *     phone: string;
     *     email: string;
     *     info: string;
     *   };
     * }
     * @returns utworzony obiekt
     */
    addObject(obj: ObjectPost): Promise<Stored<ObjectModel>>;
    /**
     * Pobiera obiekt po kluczu obcym
     * @param externalKey - klucz obcy
     */
    getObjectByExternalKey(externalKey: string): Promise<Stored<ObjectModel>>;
    /**
   * Aktualizuje obiekt
   * @param externalKey - klucz obcy
   * @param obj - dane obiektu do dodania {@link ObjectPost} {
   *   name: string;
   *   external_key: string;
   *   address: {
   *     country: string;
   *     city: string;
   *     address: string;
   *     postCode: string;
   *   };
   *   contact: {
   *     name: string;
   *     phone: string;
   *     email: string;
   *     info: string;
   *   };
   * }
   * @returns zaktualizowany obiekt
   */
    updateObjectByExternalKey(externalKey: string, obj: ObjectPost): Promise<Stored<ObjectModel>>;
    /**
     * Aktualizuje obiekt
     * @returns zaktualizowany obiekt
     */
    updateObject(obj: Stored<ObjectModel>): Promise<Stored<ObjectModel>>;
}
