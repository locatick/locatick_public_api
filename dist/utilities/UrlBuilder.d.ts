import { PaginationConfig } from "src/types/server";
export interface FetchParamsInterface {
    pagination?: PaginationConfig;
    filter?: Record<string, string | string[] | number | number[] | boolean | null | undefined> | null;
}
export declare const clearFilter: <T = Record<string, any>>(filter: T) => Partial<T>;
export declare const urlBuilder: (params?: FetchParamsInterface | null) => URLSearchParams;
