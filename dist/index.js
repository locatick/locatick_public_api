(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
  typeof define === 'function' && define.amd ? define(['exports'], factory) :
  (global = typeof globalThis !== 'undefined' ? globalThis : global || self, factory(global["Locatick API"] = {}));
})(this, (function (exports) { 'use strict';

  const locatickContext = {
      baseUrl: process.env.LOCATICK_URL ?? "https://api.locatick.cloud",
      headers: {
          Authorization: "",
      },
  };
  function mergeUrl(url) {
      if (locatickContext.baseUrl.endsWith("/"))
          return `${locatickContext.baseUrl}api/public/${url}`;
      return `${locatickContext.baseUrl}/api/public/${url}`;
  }
  const locatickApi = {
      get: async (url) => {
          const headers = {
              "Content-Type": "application/json",
              ...locatickContext.headers,
          };
          const request = await fetch(mergeUrl(url), {
              method: "GET",
              headers,
          });
          try {
              const res = (await request.json());
              return {
                  data: res,
                  status: request.status,
              };
          }
          catch (err) {
              console.error(err);
              throw err;
          }
      },
      post: async (url, body) => {
          const _body = body ? JSON.stringify(body) : "";
          const request = await fetch(mergeUrl(url), {
              method: "POST",
              headers: {
                  Accept: "application/json, text/plain, */*",
                  "Content-Type": "application/json",
                  ...locatickContext.headers,
              },
              body: _body
          });
          if (request.status >= 400) {
              const res = await request.text();
              throw new Error(res);
          }
          const res = await request.json();
          return {
              data: res,
              status: request.status,
          };
      },
      put: async (url, body) => {
          const _body = body ? JSON.stringify(body) : "";
          const request = await fetch(mergeUrl(url), {
              method: "PUT",
              headers: {
                  Accept: "application/json, text/plain, */*",
                  "Content-Type": "application/json",
                  ...locatickContext.headers,
              },
              body: _body
          });
          if (request.status >= 400) {
              const res = await request.text();
              throw new Error(res);
          }
          const res = await request.json();
          return {
              data: res,
              status: request.status,
          };
      },
  };
  function createUrl(endpoint, params) {
      if (params) {
          return `${endpoint}?${params?.toString() ?? ""}`;
      }
      else {
          return endpoint;
      }
  }
  async function locatickGetList(url, params) {
      const res = await locatickApi.get(createUrl(url, params).toString());
      return res.data["hydra:member"];
  }
  async function locatickPost(url, body) {
      return await locatickApi.post(url, body);
  }
  async function locatickPut(url, body) {
      return await locatickApi.put(url, body);
  }

  exports.ActivityStatus = void 0;
  (function (ActivityStatus) {
      ActivityStatus[ActivityStatus["STATUS_NEW"] = 10] = "STATUS_NEW";
      ActivityStatus[ActivityStatus["STATUS_ASSIGNED"] = 40] = "STATUS_ASSIGNED";
      ActivityStatus[ActivityStatus["STATUS_IN_PROGRES"] = 50] = "STATUS_IN_PROGRES";
      ActivityStatus[ActivityStatus["STATUS_TO_ACCEPT"] = 60] = "STATUS_TO_ACCEPT";
      ActivityStatus[ActivityStatus["STATUS_COMPLETED"] = 20] = "STATUS_COMPLETED";
      ActivityStatus[ActivityStatus["STATUS_ARCHIVED"] = 30] = "STATUS_ARCHIVED";
      ActivityStatus[ActivityStatus["STATUS_CANCELED"] = -10] = "STATUS_CANCELED";
      ActivityStatus[ActivityStatus["STATUS_COMMISSIONED"] = 70] = "STATUS_COMMISSIONED";
      ActivityStatus[ActivityStatus["STATUS_REJECTED"] = 80] = "STATUS_REJECTED";
  })(exports.ActivityStatus || (exports.ActivityStatus = {}));
  const ActivityPostBase = {
      tags: [],
      comments: [],
      customFields: [],
  };
  /**
   * Klasa-kontroler dla zadań
   */
  class Activity {
      constructor(model) {
          this["@id"] = undefined;
          this.address = null;
          this.description = "";
          this.id = null;
          this.name = "";
          this.object = null;
          this.owner = null;
          this.category = model.category;
          Object.assign(this, model);
      }
      /**
       * Zapisz zmiany
       * @returns model zapisanego zadania
       */
      async save() {
          if (this.isStored)
              return;
          return locatickPost(`activities/${this.id}`, this);
      }
      /**
       * Pobiera przypisanych do zadania użytkowników
       * @returns lista wykonawców zadania {@link UserModel}
       */
      async getAssignedUsers() {
          if (!this.isStored)
              return [];
          return locatickGetList(`activities/${this.id}/assigned-users`);
      }
      /**
       * Przypisuje użytkownika do zadania
       * @param user {@link UserModel} lub string z emailem
       */
      async addAssignedUser(user) {
          if (!this.isStored)
              return [];
          await locatickPost(`activities/${this.id}/assigned-users`, {
              email: typeof user == "string" ? user : user.email,
          });
      }
      /**
       * Pobiera komentarze
       * @returns lista komentarzy {@link ActivityCommentModel}
       */
      async getComments() {
          if (!this.isStored)
              return [];
          return locatickGetList(`activities/${this.id}/comments`);
      }
      /**
       * Dodaje komentarz
       * @param comment - treść komentarza
       * @param owner - autor komentarza {@link UserModel} lub email
       * @param date - data (opcjonalnie, obiekt Date)
       */
      async addComment(comment, owner, date) {
          if (!this.isStored)
              return [];
          await locatickPost(`activities/${this.id}/comments`, {
              comment,
              owner: { email: typeof owner == "string" ? owner : owner.email },
              date: date ?? new Date(),
          });
      }
      /**
       * Pobiera listę pól dodatkowych
       * @returns lista pól dodatkowych {@link CustomFieldModel}
       */
      async getCustomFields() {
          if (!this.isStored)
              return [];
          return locatickGetList(`activities/${this.id}/custom-fields`);
      }
      /**
       * Dodaje pole dodatkowe
       * @param name - nazwa pola dodatkowego
       * @param value - wartość pola dodatkowego
       * @returns
       */
      async addCustomField(name, value) {
          if (!this.isStored)
              return [];
          return await locatickPost(`activities/${this.id}/custom-fields`, {
              name,
              value,
          });
      }
      async getTags() {
          if (!this.isStored)
              return [];
          return locatickGetList(`activities/${this.id}/tags`);
      }
      async addTag(name) {
          if (!this.isStored)
              return [];
          return await locatickPost(`activities/${this.id}/tags`, {
              name,
          });
      }
      get isStored() {
          return this.id != null;
      }
  }
  function parseActivityFromResponse(src) {
      return new Activity(src);
  }

  const idFromIri = (iri) => iri.split("/").slice(-1)[0];
  const camelToUnderscore = (key) => {
      return key.replace(/([A-Z])/g, "_$1").toLowerCase();
  };
  const urlBuilder = (params) => {
      const searchParams = new URLSearchParams();
      if (params == null) {
          return searchParams;
      }
      if (params.pagination?.page) {
          searchParams.append("page", params.pagination.page.toString());
          searchParams.append("itemsPerPage", params.pagination.rowsPerPage?.toString() ?? "30");
      }
      else {
          searchParams.append("pagination", "false");
      }
      if (params.pagination?.sortBy) {
          searchParams.append("order[" + camelToUnderscore(params.pagination.sortBy.toString()) + "]", params.pagination.descending ?? false ? "desc" : "asc");
      }
      if (params.filter) {
          for (const key in params.filter) {
              if (Object.prototype.hasOwnProperty.call(params.filter, key)) {
                  const value = params.filter[key];
                  if (typeof value === "string" || Array.isArray(value)) {
                      if (value.length == 0) {
                          continue;
                      }
                  }
                  if (value != null) {
                      if (Array.isArray(value)) {
                          value.forEach((data) => {
                              if (typeof data === "object" && "@id" in data) {
                                  searchParams.append(key, idFromIri(data["@id"]));
                              }
                              else {
                                  searchParams.append(key, data);
                              }
                          });
                      }
                      else if (typeof value === "object") {
                          if ("@id" in value) {
                              searchParams.append(key, idFromIri(value["@id"]));
                          }
                      }
                      else {
                          searchParams.append(key, value.toString());
                      }
                  }
              }
          }
      }
      return searchParams;
  };

  class LocatickApi {
      constructor(key) {
          this.key = key;
          this.setActive();
      }
      setActive() {
          locatickContext.headers.Authorization = this.key;
      }
      setUrl(url) {
          locatickContext.baseUrl = url;
      }
      /*  ACTIVITIES  */
      /**
       * Pobiera zadania
       * @param filter - filtry {@link ActivityFilter}
       * @param pagination - ustawienia paginacji {@link PaginationConfig}
       * @returns lista zadań (kontrolery {@link Activity})
       */
      async getActivities(filter, pagination) {
          const params = urlBuilder({ filter, pagination });
          const url = createUrl("activities", params);
          const rawList = await locatickGetList(url.toString());
          return rawList.map(parseActivityFromResponse);
      }
      /**
       * Pobiera zadanie
       * @param id - id zadania
       * @returns zadanie (kontroler {@link Activity})
       */
      async getActivity(id) {
          const res = await locatickApi.get(createUrl(`activities/${id}`).toString());
          return parseActivityFromResponse(res.data);
      }
      /**
       * Dodaje zadanie
       * @param activity Dodanie zadania {@link ActivityPost} {
       *   name: string;
       *   description: string;
       *   address: string;
       *   number: string;
       *   owner: { name: string };
       *   category: { name: string };
       *   object: { id: number };
       *   tags: Array<{ name: string }>;
       *   comments: Array<{ comment: string; owner: string; date: Date }>;
       *   customFields: Array<{ name: string; value: string }>;
       * }
       * @returns utworzone zadanie (kontroler {@link Acitivty})
       */
      async addActivity(activity) {
          const res = await locatickApi.post(createUrl(`activities`).toString(), { ...ActivityPostBase, ...activity });
          return parseActivityFromResponse(res.data);
      }
      /*  CATEGORY  */
      /**
       * Pobiera kategorie
       * @param filter - filtry
       * @param pagination - ustawienia paginacji {@link PaginationConfig}
       * @returns lista kategorii
       */
      async getCategories(filter, pagination) {
          const params = urlBuilder({ filter, pagination });
          return await locatickGetList(createUrl("categories", params).toString());
      }
      /**
       * Pobiera kategorię wg ID
       * @param id id kategorii
       * @returns kategoria
       */
      async getCategory(id) {
          return (await locatickApi.get(`categories/${id}`)).data;
      }
      /*  CLIENT  */
      /**
       * Pobiera klientów
       * @param filter - filtry
       * @param pagination - ustawienia paginacji {@link PaginationConfig}
       * @returns lista klientów
       */
      async getClients(filter, pagination) {
          const params = urlBuilder({ filter, pagination });
          return await locatickGetList(createUrl("clients", params).toString());
      }
      /**
       * Pobiera klienta wg id
       * @param id - id klienta
       * @returns klient
       */
      async getClient(id) {
          return (await locatickApi.get(`clients/${id}`)).data;
      }
      /**
       * Dodaje klienta do bazy
       * @param client opis klienta: {@link ClientPost} {
       *   name: string;
       *   city: string;
       *   address: string;
       *   nip: string | null;
       *   regon: string | null;
       *   email: string;
       *   pcode: string;
       *   phone: string;
       *   external_key: string;
       * };
       * @returns utworzona encja
       */
      async addClient(client) {
          const res = await locatickPost("clients", client);
          return res.data;
      }
      /*  OBJECTS  */
      /**
       * Pobiera obiekty
       * @param filter - filtry
       * @param pagination  - ustawienia paginacji {@link PaginationConfig}
       * @returns lista obiektów
       */
      async getObjects(filter, pagination) {
          const params = urlBuilder({ filter, pagination });
          return await locatickGetList(createUrl("objects", params).toString());
      }
      /**
       * Pobiera obiekt
       * @param id - id obiektu
       * @returns obiekt
       */
      async getObject(id) {
          return (await locatickApi.get(`objects/${id}`)).data;
      }
      /**
       * Dodaje obiekt
       * @param obj - dane obiektu do dodania {@link ObjectPost} {
       *   name: string;
       *   external_key: string;
       *   address: {
       *     country: string;
       *     city: string;
       *     address: string;
       *     postCode: string;
       *   };
       *   contact: {
       *     name: string;
       *     phone: string;
       *     email: string;
       *     info: string;
       *   };
       * }
       * @returns utworzony obiekt
       */
      async addObject(obj) {
          const res = await locatickPost("objects", obj);
          return res.data;
      }
      /**
       * Pobiera obiekt po kluczu obcym
       * @param externalKey - klucz obcy
       */
      async getObjectByExternalKey(externalKey) {
          const res = await locatickApi.get(`objectsByExternalKey/${externalKey}`);
          return res.data;
      }
      /**
     * Aktualizuje obiekt
     * @param externalKey - klucz obcy
     * @param obj - dane obiektu do dodania {@link ObjectPost} {
     *   name: string;
     *   external_key: string;
     *   address: {
     *     country: string;
     *     city: string;
     *     address: string;
     *     postCode: string;
     *   };
     *   contact: {
     *     name: string;
     *     phone: string;
     *     email: string;
     *     info: string;
     *   };
     * }
     * @returns zaktualizowany obiekt
     */
      async updateObjectByExternalKey(externalKey, obj) {
          const res = await locatickPut(`objects/externalKey/${externalKey}`, obj);
          return res.data;
      }
      /**
       * Aktualizuje obiekt
       * @returns zaktualizowany obiekt
       */
      async updateObject(obj) {
          const res = await locatickPut(`objects/${obj.id}`, obj);
          return res.data;
      }
  }

  exports.Activity = Activity;
  exports.LocatickApi = LocatickApi;

  Object.defineProperty(exports, '__esModule', { value: true });

}));
//# sourceMappingURL=index.js.map
