import { Model, Stored } from "src/types/common";
export declare const locatickContext: {
    baseUrl: string;
    headers: {
        Authorization: string;
    };
};
export declare type Response<T> = {
    data: T;
    status: number;
};
export declare const locatickApi: {
    get: <T>(url: string) => Promise<Response<T>>;
    post: <T_1 = any, D = T_1>(url: string, body?: D | undefined) => Promise<Response<T_1>>;
    put: <T_2 = any, D_1 = T_2>(url: string, body?: D_1 | undefined) => Promise<Response<T_2>>;
};
export declare function createUrl(endpoint: string, params?: URLSearchParams): string;
export declare function locatickGetList<T extends Model>(url: string, params?: URLSearchParams): Promise<Stored<T>[]>;
export declare function locatickPost<T extends Model, D = any>(url: string, body?: D): Promise<Response<T>>;
export declare function locatickPut<T extends Model, D = any>(url: string, body?: D): Promise<Response<T>>;
